import './App.css';
import Home from "./views/Home";
import PageNotFound from './views/PageNotFound';
import Booking from './views/Booking';
import Detail from './views/Detail';
import Profile from './views/Profile';
import { Fragment, useEffect } from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { useDispatch } from 'react-redux';
import {fetchMe} from './store/actions/auth';
import {PrivateRoute } from "./HOCs/Route";

function App() {
  const dispatch = useDispatch();
  const token = localStorage.getItem("token");
  const taikhoan = localStorage.getItem("taikhoan");
  useEffect(() => {
    if(token) (dispatch(fetchMe(taikhoan)))
 }, [dispatch,token]);

  return (
    <Fragment>
    <BrowserRouter>
        <Switch>
          <PrivateRoute path="/profile" exact component={Profile} redirectPath="/" />
          <Route path="/detail/:id" exact component={Detail} />
          <PrivateRoute path="/booking/:id" exact component={Booking}  redirectPath="/"/>
          <Route path="/" exact component={Home} />
          <Route path="*" redirectPath="/" component={PageNotFound} />
        </Switch>
    </BrowserRouter>
    </Fragment>
  );
}

export default App;
