import Logo from "../assets/logo.png";
import BHD from "../assets/cinema/bhd.png";
import BT from "../assets/cinema/bt.jpg";
import CGV from "../assets/cinema/cgv.png";
import Cinestar from "../assets/cinema/cinestar.png";
import CNX from "../assets/cinema/cnx.jpg";
import Dcine from "../assets/cinema/dcine.png";
import Glaxy from "../assets/cinema/galaxycine.png";
import Lotte from "../assets/cinema/lotte.png";
import Mega from "../assets/cinema/megags.png";
import Startlight from "../assets/cinema/STARLIGHT.png";
import Touch from "../assets/cinema/TOUCH.png";
import Apple from "../assets/app-store.png";
import Google from "../assets/google-play.png";
import Hot from "../assets/hot.png";
import Screen from "../assets/screen.png";
import imgDefault from "../assets/image_default.png";
import Banner1 from "../assets/TrangTi_1200x500.jpg";
import Banner2 from "../assets/1990_1440wx600h.jpg";
import Banner3 from "../assets/thosan_1920X800.jpg";
import ImgNotFound from "../assets/img-not-found.png"

const images = {
    Logo,
    BT,
    BHD,
    CGV,
    Cinestar,
    CNX,
    Dcine,
    Glaxy,
    Lotte,
    Mega,
    Startlight,
    Touch,
    Apple,
    Google,
    Hot,
    Screen,
    imgDefault,
    Banner1,
    Banner2,
    Banner3,
    ImgNotFound,
}
export default images;