import {React, Fragment} from 'react';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

function Layout(props) {
    return (
        <Fragment>
            <Header signOut={props.signOut} />
                {props.children}
            <Footer />
        </Fragment>
    );
}

export default Layout;