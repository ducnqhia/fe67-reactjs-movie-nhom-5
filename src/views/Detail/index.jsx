import React, { useEffect,useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Layout from "../../HOCs/Layout";
import moment from "moment";
import "./styles.css";
import { fetchCinemaByFilm } from "../../store/actions/cinemas";
import { message,Tabs } from "antd";
import { NavLink } from "react-router-dom";
import { Anchor, Button  } from 'antd';
import images from "../../assets";
import "./modal-video.min.css";
import ModalVideo from 'react-modal-video';

function Detail(props) {
  const dispatch = useDispatch();
  const [isOpen, setOpen] = useState(false)

  const {ImgNotFound} = images;
  useEffect(() => {
    const maPhim = props.match.params.id;
    dispatch(fetchCinemaByFilm(maPhim));
  }, [dispatch,props.match.params.id]);

  const { Link } = Anchor;
  const { TabPane } = Tabs;

  const movie = useSelector((state) => {
    return state.cinemas.cinemaMovie || [];
  });
  
  const user = useSelector((state) => {
    return state.user || [];
  });
  
  const warning = () => {
    message.warning('Vui lòng đăng nhập để đặt vé xem phim', 4);
  };

  const renderMovie = () => {
      return (
        <>
          {/* <div className="movie-detail-img" style={{backgroundImage: `url('${movie?.hinhAnh}')`}}> */}
          <div className="movie-detail-img">
            <img className="" src={movie?.hinhAnh} alt={movie?.tenPhim}
            onError={(e) => {
                e.target. src = ImgNotFound  //replacement image imported above
                e.target.style = 'height:20rem; width:100%' // inline styles in html format
            }}
             />
          </div>

          <div className="lg:col-span-3 sm:col-span-2 bg-black opacity-90 px-4">
            <h2 className="uppercase font-bold text-md  my-2 tracking-widest">
              {movie?.tenPhim}
            </h2>
            <div className="flex text-white text-base">
            {movie?.danhGia>=10 ? (
              <span className="bg-red-500 sm:text-md text-sm sm:mr-2 mr-1 sm:p-1 p-0.5 rounded sm:font-bold font-medium">HOT</span>
            ) : (
              <div></div>
            )}
            {movie.dangChieu ? (
              <div className="bg-gray-500 sm:mr-2 mr-1 sm:p-1 p-0.5 rounded text-center sm:text-md text-sm ">Coming Soon</div>
            ) : (
              <div className="bg-green-500 sm:mr-2 mr-1 sm:p-1 p-0.5 rounded text-center sm:text-md text-sm  ">In Theaters</div>
            )}
            </div>
            <div className=" my-2">
              <span className="movies-overerate">
                {
                  <>
                    {[...Array(movie.danhGia)].map((e, i) => {
                      return (
                        <img
                          className="sm:w-4 w-3 inline"
                          src="https://img.icons8.com/fluency/48/000000/star.png"
                          alt="Rate"
                        />
                      );
                    })}
                  </>
                }
                {movie.danhGia < 10 ? (
                  <>
                    {[...Array(10 - movie.danhGia)].map((e, i) => {
                      return (
                        <img
                          className="sm:w-4 w-3"
                          src="https://img.icons8.com/color/48/000000/star--v1.png"
                          alt="Rate"
                        />
                      );
                    })}
                  </>
                ) : (
                  ""
                )}
              </span>
            </div>
            <p className="movie-detail-desc text-justify text-white my-2 tracking-wide">
              {movie.moTa}
            </p>
            <h4 className="tracking-wide sm:text-md text-xs">
              Released: {moment(movie?.ngayKhoiChieu).format("hh:mm A DD/MM ")}
            </h4>
            <div>
              <Anchor>
                <div className="flex items-center mb-2">
                <Link href="#bookingMovie" className="sm:text-md text-sm movie-detail-button tracking-widest bg-primary text-white rounded uppercase font-bold mr-2" title="booking"/>
                <Button className="sm:text-md text-sm btn-trailer rounded uppercase font-bold" onClick={()=> setOpen(true)}>trailer</Button>      
                </div>
              </Anchor>
            </div>
          </div>
        </>
      );
    // });
  };
  const renderSchedule = () => {
    return(
    <div id="bookingMovie" className="container py-4 bg-white">
      <Tabs defaultActiveKey="1" centered className="shadow m-2">
        {movie?.heThongRapChieu?.map((heThongRap) => {
          return (
            <TabPane
              tab={
                <img
                  className="w-20 opacity-80 cursor-pointer shadow bg-white rounded-full cinema-logo"
                  src={heThongRap.logo}
                  alt={heThongRap.tenHeThongRap}
                />
              }
              key={heThongRap.maHeThongRap}
            >
              <Tabs  id="showtime-schedule" className="h-80" tabPosition="left" defaultActiveKey="1" centered>
                {
                  heThongRap.cumRapChieu.map((cumRap)=>{
                    return <TabPane  key={cumRap.maCumRap} tab= {
                    <div className="p-2  flex "> 
                      <img
                        className="content-center w-10 h-10 cursor-pointer shadow bg-white rounded-full cinema-logo mr-2"
                        src={heThongRap.logo}
                        alt={heThongRap.tenHeThongRap}
                      />
                      {/* <div className="showtime-cinema-name"> */}
                        <p className="font-bold cinemas-name">{cumRap.tenCumRap}</p>
                        {/* <p>{cumRap.diaChi}</p> */}
                        {/* </div> */}
                        </div>
                       }>
                       <div>
                       {cumRap.lichChieuPhim.map((film)=>{
                         return  (
                          <div className="mb-2 border-b border-gray-100 md:px-6 pl-3 pr-6">
                          <div className="sm:flex block cinema-desc py-2">
                          <div className="movie-detail-booking-img w-20 mr-2 my-1">
                            <img src={movie.hinhAnh} alt={movie.tenPhim}
                              onError={(e) => {
                                e.target. src = ImgNotFound  //replacement image imported above
                                e.target.style = 'height:100%; width:100%' // inline styles in html format
                            }}
                            />
                          </div>
                           {/* <div className="movie-detail-booking-img sm:w-20 w-full mr-2 my-1" style={{backgroundImage: `url('${movie.hinhAnh}')`}}>                                      </div> */}
                            <div>
                              <div className="sm:flex block sm:mb-2 mb-0.5 my-auto items-center">
                               <span className="bg-green-400 uppercase text-white font-semibold mr-2 rounded sm:p-1 p-0.5 text-center">{film.tenRap}</span>
                                <h3 className="mr-2 capitalize sm:text-2xl text-xl font-medium m-0">{movie.tenPhim}</h3>
                              </div>
                              <div className="md:flex block sm:my-4 my-2">
                                  <p className="m-0 px-2 md:border-r-2 border-r-0 ">Thời lượng: <span className="font-bold">{film.thoiLuong} phút</span></p>
                                  <p className="m-0 px-2 md:border-r-2 border-r-0">Giá vé:  <span className="font-bold">{film.giaVe} đ</span></p>
                                  <p className="m-0 px-2  ">Giờ chiếu:  <span className="font-bold">{moment(film.ngayChieuGioChieu).format("hh:mm A")}</span></p>                           
                              </div>
                              {Object.keys(user).length>0?
                                <NavLink to={`/booking/${film.maLichChieu}`}
                                    id="btn-booking"
                                    className="transition-all duration-50 bg-primary sm:p-2 p-1 text-md text-bold font-semibold text-center rounded"
                                    key={film.maLichChieu}>BOOKING!</NavLink>:
                                <button onClick={warning} className="transition-all hover:text-white duration-50 bg-primary sm:p-2 p-1 text-md text-bold font-semibold text-center rounded" key={film.maLichChieu} >
                                  BOOKING!
                                </button>}
                            </div>
                          </div>
                        </div>
                         )
                       })}
                       </div>
                  </TabPane>
                  })
                }
              </Tabs>
            </TabPane>
          );
        })}
      </Tabs>
    </div>)
  }
  return (
    <Layout>
      <div className="movie-detail pt-28 pb-4 container grid lg:grid-cols-4 sm:grid-cols-3 grid-cols-1 gap-4">
       <div style={{backgroundImage: `url('${movie?.hinhAnh}')`}} className="movie-detail-bg"></div>
        {renderMovie()}
      </div>
      {renderSchedule()}
      <ModalVideo channel='youtube' youtube={{mute:1,autoplay:1}}  isOpen={isOpen} videoId={movie.trailer} onClose={() => setOpen(false)} />
    </Layout>
  );
}

export default Detail;
