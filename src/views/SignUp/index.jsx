import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import { signUp } from "../../store/actions/auth";
import { useTranslation } from 'react-i18next';

const signUpUserSchema = yup.object().shape({
  taiKhoan: yup.string().required("*Field is required!"),
  matKhau: yup.string().required("*Field is required!"),
  email: yup.string().required("*Field is required!").email("*Email is invalid"),
  soDt: yup
    .string()
    .required("*Field is required!")
    .matches(/^[0-9]+$/, "*Phone number is not valid"),
  maNhom: yup.string().required("*Field is required!"),
  hoTen: yup.string().required("*Field is required!"),
});

function SignUp(props) {
  const { setSignUp, isSignUp, setSignIn, setNotification  } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleClose = () => {
    setSignUp(false);
  };
  const handleOpenSignIn = () => {
    setSignIn(true);
    handleClose();
  };
  const openNotification = (msg) =>{
    setNotification(msg);
  }
  const handleSignUp = (values) => {
    dispatch(signUp(values, handleClose, openNotification));
  };
  return (
    <>
      {isSignUp ? (
        <>
          <Formik
            initialValues={{
              taiKhoan: "",
              matKhau: "",
              email: "",
              soDt: "",
              maNhom: "GP00",
              hoTen: "",
            }}
            onSubmit={handleSignUp}
            validationSchema={signUpUserSchema}
            render={({ handleChange }) => (
              <Form className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
                <div className="relative lg:w-2/5 md:w-3/5 w-4/5 my-6 mx-auto max-w-3xl">
                  {/*content*/}
                  <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                    {/*header*/}
                    <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                      <h3 className="uppercase text-3xl font-semibold">{t('content.SignUp')}</h3>
                      <button
                        className="p-1 ml-auto border-0 text-black float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                        onClick={handleClose}
                      >
                        <span className="bg-transparent text-black opacity-20 h-6 w-6 text-2xl block outline-none focus:outline-none">
                          ×
                        </span>
                      </button>
                    </div>
                    {/*body*/}
                    <div className="relative p-6 flex-auto">
                      <div className="w-full mb-2">
                        <div className="items-center">
                          <label className="block text-gray-700 text-sm font-bold mb-2">
                          {t('content.UserName')}
                          </label>
                          <Field
                            type="text"
                            name="taiKhoan"
                            onChange={handleChange}
                            placeholder={t('content.UserName')}
                            className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                          />
                          <ErrorMessage  name="taiKhoan">
                            {(msg) => <p className="text-red-500 text-xs">{msg}</p>}
                          </ErrorMessage>
                        </div>
                      </div>

                      <div className="w-full mb-2">
                        <div className="items-center">
                          <label className="block text-gray-700 text-sm font-bold mb-2">
                          {t('content.Password')}
                          </label>
                          <Field
                            type="text"
                            name="matKhau"
                            onChange={handleChange}
                            placeholder=   {t('content.Password')}
                            className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                          />
                          <ErrorMessage name="matKhau">
                            {(msg) => <p className="text-red-500 text-xs">{msg}</p>}
                          </ErrorMessage>
                        </div>
                      </div>

                      <div className="w-full mb-2">
                        <div className="flex justify-between gap-x-4">
                          <div className=" flex-grow">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                            {t('content.Name')}
                            </label>
                            <Field
                              type="text"
                              name="hoTen"
                              onChange={handleChange}
                              placeholder= {t('content.Name')}
                              className="outline-none shadow p-1 w-full border rounded text-gray-700"
                            />
                            <ErrorMessage name="hoTen">
                              {(msg) => <p className="text-red-500 text-xs">{msg}</p>}
                            </ErrorMessage>
                          </div>
                          <div className="flex-grow-0">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                            {t('content.Group')}
                            </label>
                            <div className="cursor-pointer shadow px-2 w-full border rounded py-1 text-gray-700">
                              <select
                                className="cursor-pointer"
                                onChange={handleChange}
                                name="maNhom"
                              >
                                <option value="GP00">GP00</option>
                                <option value="GP01">GP01</option>
                                <option value="GP02">GP02</option>
                                <option value="GP03">GP03</option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div className="w-full mb-2">
                        <div className=" flex gap-x-4">
                          <div className=" w-full">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                              Email
                            </label>
                            <Field
                              type="text"
                              name="email"
                              onChange={handleChange}
                              placeholder="Email"
                              className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                            />
                            <ErrorMessage name="email">
                              {(msg) => <p className="text-red-500 text-xs">{msg}</p>}
                            </ErrorMessage>
                          </div>
                          <div className=" w-full">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                            {t('content.PhoneNumber')}
                            </label>
                            <Field
                              type="text"
                              name="soDt"
                              onChange={handleChange}
                              placeholder=     {t('content.PhoneNumber')}
                              className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                            />
                            <ErrorMessage name="soDt">
                              {(msg) => <p className="text-red-500 text-xs">{msg}</p>}
                            </ErrorMessage>
                          </div>
                        </div>
                      </div>

                      <div className="mt-2 flex justify-end">
                        <div>
                          <p className=" text-gray-700">
                          {t('content.Alreadyhaveanaccount')}{" "}
                            <span
                              onClick={handleOpenSignIn}
                              className=" cursor-pointer text-primary"
                              href=""
                            >
                              {t('content.SignIn')}
                            </span>
                          </p>
                        </div>
                      </div>
                      <button
                        type="text"
                        className="uppercase hover:opacity-80 w-full mt-6 font-medium bg-primary text-gray-700 rounded px-4 py-2"
                      >
                        {t('content.SignUp')}
                      </button>
                    </div>
                  </div>
                </div>
              </Form>
            )}
          />
        </>
      ) : null}
    </>
  );
}

export default SignUp;
