import React from "react";

function Notification(props) {
  const { isNotification, setNotification} = props;

  return (
    <>
      {isNotification!=="false"  ? (
        <div className=" justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
          {isNotification.includes("Chúc mừng")? (
            <div
              className="sm:w-2/6 w-5/6 bg-white border-blue-500 border-t-4 rounded-b px-4 py-3 shadow-md"
              role="alert"
            >
              <div className="flex">
                <div className="py-1 pr-2 text-blue-500">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="icon icon-tabler icon-tabler-circle-check"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    strokeWidth="2"
                    stroke="currentColor"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <circle cx="12" cy="12" r="9"></circle>
                    <path d="M9 12l2 2l4 -4"></path>
                  </svg>
                </div>
                <div className="text-gray-700 ">
                  <p className="font-bold mb-1 text-blue-500">SUCCESS!</p>
                  <p className="text-sm mb-1">{isNotification}</p>
                </div>
              </div>
              <div className="flex items-center justify-end rounded-b mt-1">
                <button
                  className="text-white bg-blue-500 px-2 py-1 rounded font-bold uppercase text-sm outline-none focus:outline-none ease-linear transition-all duration-150"
                  type="button"
                  onClick={()=>setNotification("false")}
                >
                  OK
                </button>
              </div>
            </div>
          ) : (
            <div
              className="sm:w-2/6 w-5/6 bg-white border-red-500 border-t-4 rounded-b px-4 py-3 shadow-md"
              role="alert"
            >
              <div className="flex">
                <div className="py-1 pr-2 text-red-500">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="icon icon-tabler icon-tabler-alert-triangle"
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    strokeWidth="2"
                    stroke="currentColor"
                    fill="none"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  >
                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                    <path d="M12 9v2m0 4v.01"></path>
                    <path d="M5 19h14a2 2 0 0 0 1.84 -2.75l-7.1 -12.25a2 2 0 0 0 -3.5 0l-7.1 12.25a2 2 0 0 0 1.75 2.75"></path>
                  </svg>
                </div>
                <div className="text-gray-700 ">
                  <p className="font-bold mb-1 text-red-500">FAIL!</p>
                  <p className="text-sm mb-1">{isNotification}</p>
                </div>
              </div>
              <div className="flex items-center justify-end rounded-b mt-1">
                <button
                  className="text-white bg-red-500 px-2 py-1 rounded font-bold uppercase text-sm outline-none focus:outline-none ease-linear transition-all duration-150"
                  type="button"
                  onClick={() => setNotification("false")}
                >
                  OK
                </button>
              </div>
            </div>
          )}
        </div>
      ) : null}
    </>
  );
}

export default Notification;
