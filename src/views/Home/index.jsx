import React from 'react';
import Layout from '../../HOCs/Layout';
import Application from '../../components/Application';
import CarouselBanners from '../../components/CarouselBanners';
import ShowTimes from '../../components/ShowTimes';
import "./styles.css";
import Movies from '../../components/Movies';
import { BackTop } from 'antd';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faChevronUp} from "@fortawesome/free-solid-svg-icons";

function Home(props) {
    return (
        <Layout>
            <CarouselBanners/>
            <Movies />
            <ShowTimes/>
            <Application />
            <BackTop>
            <FontAwesomeIcon className="bg-primary text-white text-3xl" icon={faChevronUp} />
    </BackTop>
        </Layout>
    );
}

export default Home;