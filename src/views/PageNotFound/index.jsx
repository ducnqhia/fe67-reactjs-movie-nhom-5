import React from 'react';
import Layout from '../../HOCs/Layout';
import { Result } from 'antd';
import { NavLink } from 'react-router-dom';

function PageNotFount(props) {
    return (
        <Layout>
            <div className="pt-16 bg-white">
               <Result
    status="404"
    title="404"
    subTitle="Sorry, the page you visited does not exist."
    extra={ <NavLink to="" className="bg-white hover:text-white hover:bg-primary rounded border p-2 ">Back Home
    
  </NavLink>}
  /></div>
        </Layout>
    );
}

export default PageNotFount;