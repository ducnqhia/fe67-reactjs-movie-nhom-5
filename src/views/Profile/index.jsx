import React, { useState } from "react";
import Layout from "../../HOCs/Layout";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { updateUser } from "../../store/actions/auth";
import { Avatar, message } from "antd";
import { UserOutlined } from '@ant-design/icons';
import moment from "moment";
import "./styles.css";
import { Collapse } from "antd";
import images from "../../assets";

const signUpUserSchema = yup.object().shape({
  taiKhoan: yup.string().required("*Field is required!"),
  matKhau: yup.string().required("*Field is required!"),
  email: yup
    .string()
    .required("*Field is required!")
    .email("*Email is invalid"),
  soDt: yup
    .string()
    .required("*Field is required!")
    .matches(/^[0-9]+$/, "*Phone number is not valid"),
  maNhom: yup.string().required("*Field is required!"),
  hoTen: yup.string().required("*Field is required!"),
});
function Profile(props) {
  const _ = require("lodash");
  const { Panel } = Collapse;
  const dispatch = useDispatch();
  const [isEnableBtn, setEnableBtn] = useState(true);
  const {ImgNotFound } = images;
  const handleEdit = () => {
    setEnableBtn(false);
  };

  const handleSave = () => {
    setEnableBtn(true);
  };
  const handleUpdate = (values) => {
    console.log(values);
    dispatch(updateUser(values, info));
  };

  const info = (msg) => {
    message.info(msg);
  };
  const {
    taiKhoan,
    matKhau,
    email,
    soDT,
    hoTen,
    maNhom,
    maLoaiNguoiDung,
    thongTinDatVe,
  } = useSelector((state) => {
    return state.user || [];
  });

  const renderHistory = () => {
    let thongTinDatVeClone = [...thongTinDatVe]?.reverse();
    return thongTinDatVeClone?.map((item) => {
      return (
        <div className="flex shadow my-2 ">
          <img
            className="w-14 h-20 my-auto m-2"
            src={ImgNotFound}
            onError={(e) => {
              e.target.src = ImgNotFound  //replacement image imported above
              // e.target.style = 'height:20rem; width:100%' // inline styles in html format
          }}
            alt={item?.tenPhim}
           
          />
          <div className="">
            <p className="profile-movie-name m-0 uppercase tracking-wide text-md font-semibold">
              {item?.tenPhim}
            </p>
            <span className="sm:inline block m-0 px-2 text-black ">
              Giá vé:{" "}
              <span className="font-bold">{item?.giaVe.toLocaleString()}đ</span>
            </span>
            <span className="sm:inline block m-0 px-2 text-black sm:border-l sm:border-r border-0">
              Thời lượng:{" "}
              <span className="font-bold">{item?.thoiLuongPhim}'</span>
            </span>
            <span className="sm:inline block m-0 px-2 text-black">
              Ngày đặt:{" "}
              <span className="font-bold">
                {moment(item?.ngayDat).format(" hh:mm A DD/MM/YY ")}
              </span>
            </span>

            <Collapse bordered={false}>
              <Panel
                className="text-primary"
                header="Danh sách ghế đã đặt:"
                key="1"
              >
                <ul className="list-disc">
                  {_.sortBy(item.danhSachGhe, ["tenGhe"]).map((seat) => {
                    return (
                      <li className="sm:text-md text-xs mt-0.5">
                        Ghế: {seat.tenGhe}, {seat.tenRap}, {seat.tenHeThongRap}{" "}
                      </li>
                    );
                  })}{" "}
                </ul>
              </Panel>
            </Collapse>
          </div>
        </div>
      );
    });
  };
  return (
    <Layout>
      <div className="movie-profile bg-white ">
        <div className="container pt-24 pb-10">
          <div className="grid lg:grid-cols-2 grid-cols-1">
            <div className="">
              <h2 className="uppercase text-2xl mt-2">profile</h2>
              <div className="flex profile-detail border px-4">
                <div className=" sm:block hidden mr-4 mt-6 flex-grow">
                  <Avatar style={{ backgroundColor: "#FF9900", width:"8rem",height:"8rem", fontSize:"8rem" }} icon={<UserOutlined />} />
                </div>
                <Formik
                  initialValues={{
                    taiKhoan: taiKhoan,
                    matKhau: matKhau,
                    email: email,
                    soDt: soDT,
                    maNhom: maNhom,
                    maLoaiNguoiDung: maLoaiNguoiDung,
                    hoTen: hoTen,
                  }}
                  onSubmit={handleUpdate}
                  validationSchema={signUpUserSchema}
                  render={({ handleChange }) => (
                    <Form className="justify-center items-center flex  outline-none focus:outline-none ">
                      <div className="relative my-2 mx-auto max-w-3xl">
                        <div className="border-0 rounded-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                          <div className="relative flex-auto">
                            <div className="w-full mb-2">
                              <div className="items-center">
                                <label className="block text-gray-700 text-sm font-bold mb-2">
                                  Username
                                </label>
                                <Field
                                  disabled
                                  type="text"
                                  name="taiKhoan"
                                  placeholder="Username"
                                  className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                                />
                                <ErrorMessage name="taiKhoan">
                                  {(msg) => (
                                    <p className="text-red-500 text-xs">
                                      {msg}
                                    </p>
                                  )}
                                </ErrorMessage>
                              </div>
                            </div>

                            <div className="w-full mb-2">
                              <div className="items-center">
                                <label className="block text-gray-700 text-sm font-bold mb-2">
                                  Password
                                </label>
                                <Field
                                  disabled={isEnableBtn ? "disabled" : ""}
                                  type="text"
                                  name="matKhau"
                                  onChange={handleChange}
                                  placeholder="Password"
                                  className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                                />
                                <ErrorMessage name="matKhau">
                                  {(msg) => (
                                    <p className="text-red-500 text-xs">
                                      {msg}
                                    </p>
                                  )}
                                </ErrorMessage>
                              </div>
                            </div>

                            <div className="w-full mb-2">
                              <div className="flex justify-between gap-x-4">
                                <div className=" flex-grow">
                                  <label className="block text-gray-700 text-sm font-bold mb-2">
                                    Name
                                  </label>
                                  <Field
                                    disabled={isEnableBtn ? "disabled" : ""}
                                    type="text"
                                    name="hoTen"
                                    onChange={handleChange}
                                    placeholder="Name"
                                    className="outline-none shadow p-1 w-full border rounded text-gray-700"
                                  />
                                  <ErrorMessage name="hoTen">
                                    {(msg) => (
                                      <p className="text-red-500 text-xs">
                                        {msg}
                                      </p>
                                    )}
                                  </ErrorMessage>
                                </div>
                                <div className="flex-grow-0">
                                  <label className="block text-gray-700 text-sm font-bold mb-2">
                                    Permission
                                  </label>
                                  <div className="cursor-pointer shadow px-2 w-full border rounded py-1 text-gray-700">
                                    <select
                                      defaultValue={maLoaiNguoiDung}
                                      disabled={isEnableBtn ? "disabled" : ""}
                                      className="cursor-pointer"
                                      onChange={handleChange}
                                      name="maLoaiNguoiDung"
                                    >
                                      <option value="KhachHang">
                                        Khách hàng
                                      </option>
                                      <option value="QuanTri">Quản trị</option>
                                    </select>
                                  </div>
                                </div>
                                <div className="flex-grow-0">
                                  <label className="block text-gray-700 text-sm font-bold mb-2">
                                    Group
                                  </label>
                                  <div className="cursor-pointer shadow px-2 w-full border rounded py-1 text-gray-700">
                                    <select
                                      defaultValue={maNhom}
                                      disabled={isEnableBtn ? "disabled" : ""}
                                      className="cursor-pointer"
                                      onChange={handleChange}
                                      name="maNhom"
                                    >
                                      <option value="GP00">GP00</option>
                                      <option value="GP01">GP01</option>
                                      <option value="GP02">GP02</option>
                                      <option value="GP03">GP03</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div className="w-full mb-2">
                              <div className=" flex gap-x-4">
                                <div className=" w-full">
                                  <label className="block text-gray-700 text-sm font-bold mb-2">
                                    Email
                                  </label>
                                  <Field
                                    disabled={isEnableBtn ? "disabled" : ""}
                                    type="text"
                                    name="email"
                                    onChange={handleChange}
                                    placeholder="Email"
                                    className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                                  />
                                  <ErrorMessage name="email">
                                    {(msg) => (
                                      <p className="text-red-500 text-xs">
                                        {msg}
                                      </p>
                                    )}
                                  </ErrorMessage>
                                </div>
                                <div className=" w-full">
                                  <label className="block text-gray-700 text-sm font-bold mb-2">
                                    Phone Number
                                  </label>
                                  <Field
                                    disabled={isEnableBtn ? "disabled" : ""}
                                    type="text"
                                    name="soDt"
                                    onChange={handleChange}
                                    placeholder="Phone Number"
                                    className="outline-none shadow px-2 w-full border rounded py-1 text-gray-700"
                                  />
                                  <ErrorMessage name="soDt">
                                    {(msg) => (
                                      <p className="text-red-500 text-xs">
                                        {msg}
                                      </p>
                                    )}
                                  </ErrorMessage>
                                </div>
                              </div>
                            </div>
                            {isEnableBtn ? (
                              <button
                                onClick={handleEdit}
                                type="submit"
                                className="w-full mt-2 uppercase hover:opacity-80 font-medium bg-primary text-white rounded px-4 py-2"
                              >
                                edit
                              </button>
                            ) : (
                              <button
                                onClick={handleSave}
                                type="button"
                                className="w-full mt-2 uppercase hover:opacity-80  font-medium bg-primary text-white rounded px-4 py-2"
                              >
                                save
                              </button>
                            )}
                          </div>
                        </div>
                      </div>
                    </Form>
                  )}
                />
              </div>
            </div>
            <div>
              <h2 className=" uppercase text-2xl mt-2">history</h2>
              <div className="profile-history border">{renderHistory()}</div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default Profile;
