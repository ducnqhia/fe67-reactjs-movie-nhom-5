import React, { useEffect, useState } from "react";
import Layout from "../../HOCs/Layout";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCouch } from "@fortawesome/free-solid-svg-icons";
import "./styles.css";
import { layThongTinPhongChieu, booking } from "../../store/actions/booking";
import { useDispatch, useSelector } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import images from "../../assets";
import { Result } from "antd";
import { NavLink } from "react-router-dom";
import {fetchMe} from '../../store/actions/auth'

function Booking(props) {
  const dispatch = useDispatch();
  const _ = require("lodash");
  const [visible, setVisible] = useState(false);

  const maPhongChieu = props.match.params.id;
  useEffect(() => {
    dispatch(layThongTinPhongChieu(maPhongChieu));
  }, [dispatch,maPhongChieu]);

  const listBooking = useSelector((state) => {
    return state.booking.listBooking || [];
  });
  const user = useSelector((state) => {
    return state.user || [];
  });
  const upHistory = () => {
    dispatch(fetchMe);
  }
  const danhSachGheDangDat = useSelector((state) => {
    return state.booking.danhSachGheDangDat || [];
  });
  const taikhoan = localStorage.getItem("taikhoan");
  let thongTinDatVe = {
    maLichChieu: maPhongChieu,
    danhSachVe: [...danhSachGheDangDat],
    taiKhoanNguoiDung:taikhoan
  };
  const datVe = () => {
    dispatch(booking(thongTinDatVe));
    setVisible(true);
  };

  const renderSeats = () => {
    return listBooking.danhSachGhe?.map((item, index) => {
      let classGheVip = item.loaiGhe === "Vip" ? "text-red-500" : "";
      let classGheDaDat =
        item.daDat === true
          ? " text-blue-900 opacity-100 cursor-not-allowed"
          : "";
      let classGhe = "text-gray-300  text-3xl opacity-80 hover:opacity-100";
      let classGheDangDat = "";
      let indexGheDD = danhSachGheDangDat.findIndex(
        (gheDD) => gheDD.maGhe === item.maGhe
      );
      if (indexGheDD !== -1) {
        classGheDaDat = "text-green-500";
      }
      return (
        <>
          <button
            disabled={item.daDat}
            onClick={() => selectSeat(item)}
            key={index}
            title={`Ghế ${item?.tenGhe}`}
            style={{ margin: "0 0.20rem" }}
            className="text-center "
          >
            <FontAwesomeIcon
              className={`${classGhe} ${classGheVip} ${classGheDaDat} ${classGheDangDat}  `}
              icon={faCouch}
            />
            <p className="text-xs text-gray-700">{item?.tenGhe}</p>
          </button>
        </>
      );
    });
  };
  const renderInfoMovie = () => {
    return (
      <div>
        <div className="shadow lg:grid flex xl:grid-cols-3 lg:grid-cols-1 ">
          <div
            className="lg:w-full w-2/5 h-40 movie-booking-img"
            style={{
              backgroundImage: `url('${listBooking?.thongTinPhim?.hinhAnh}')`,
            }}
          ></div>
          <div className="px-2 col-span-2 text-gray-700">
            <p className="uppercase text-xl text-primary">
              <span className="bg-green-500 px-2 rounded text-white">
                {listBooking?.thongTinPhim?.tenRap}
              </span>{" "}
              {listBooking?.thongTinPhim?.tenPhim}
            </p>
            <p>
              Ngày chiếu:{" "}
              <span className="font-bold">
                {listBooking?.thongTinPhim?.ngayChieu}
              </span>
            </p>
            <p>
              Giờ chiếu:{" "}
              <span className="font-bold">
                {listBooking?.thongTinPhim?.gioChieu}
              </span>
            </p>
          </div>
        </div>
        {/* ----
         */}
          <div className="shadow p-2 mt-4">
           <p className="font-bold text-xl m-0">Info:</p>
           <ul className="list-disc ml-4">
             <li>Full Name: {user.hoTen}</li>
             <li>Phone Number: {user.soDT}</li>
             <li>Email: {user.email}</li>
           </ul>
        </div>

         {/* ---- */}
        <div className="shadow mt-4 flex ">
          <div className="w-2/5 border-r p-2 ">
            <p className="text-gray-600 m-0">Ghế đang chọn: </p>
            <div className="flex flex-wrap mx-auto">
              {_.sortBy(danhSachGheDangDat, ["stt"]).map((item, index) => {
                return (
                  <span
                    key={index}
                    className="border rounded w-8 m-1 p-1 text-center"
                  >
                    {item.stt}
                  </span>
                );
              })}
            </div>
          </div>
          <div className="w-3/5 h-full">
            <p className="text-gray-600 m-0 p-2">Giá vé:</p>
            <div className="h-full mb-4 ">
              <div className="mx-auto ">
                <p className="text-center lg:text-4xl text-2xl font-medium my-0">
                  {danhSachGheDangDat
                    .reduce((tongTien, ghe, index) => {
                      return (tongTien += ghe.giaVe);
                    }, 0)
                    .toLocaleString()}{" "}
                  Đ
                </p>
              </div>
            </div>
          </div>
        </div>
        {danhSachGheDangDat.length !== 0 ? (
          <button
            onClick={datVe}
            className="my-4 border border-gray-400 text-2xl font-bold w-full transition-all duration-500 hover:bg-white hover:text-primary uppercase bg-primary text-white py-1 px-2 rounded"
          >
            booking
          </button>
        ) : (
          <button className="my-4 cursor-not-allowed border text-2xl font-bold border-gray-400 w-full bg-gray-400 text-white py-1 px-2 rounded uppercase">
            booking
          </button>
        )}
      </div>
    );
  };

  const selectSeat = (gheDuocChon) => {
    dispatch(createAction(actionType.ADD_SEAT, gheDuocChon));
  };
  const renderInfoCinema = () => {
    return (
      <div className="text-center">
        <p className="text-2xl m-0">{listBooking?.thongTinPhim?.tenCumRap}</p>
        <p className="text-black">
          Address: {listBooking?.thongTinPhim?.diaChi}{" "}
        </p>
      </div>
    );
  };
  return (
    <Layout>
      {!visible ? (
        <div className="container bg-white pt-28">
          {renderInfoCinema()}
          <div className="grid  lg:grid-cols-12 grid-cols-1 gap-x-8">
            <div className="lg:col-span-8 col-span-1">
              <img
                className="w-11/12 mx-auto mb-4"
                src={images.Screen}
                alt="Screen"
              />
              <div className="text-center">
                {renderSeats()}
                <p className="flex flex-wrap justify-center my-4">
                  <span className="mx-1">
                    <FontAwesomeIcon
                      className="text-red-500 text-2xl mr-2"
                      icon={faCouch}
                    />
                    Ghế VIP
                  </span>
                  <span className="mx-1">
                    <FontAwesomeIcon
                      className="text-gray-300 text-2xl mr-2"
                      icon={faCouch}
                    />
                    Ghế trống
                  </span>
                  <span className="mx-1">
                    <FontAwesomeIcon
                      className="text-blue-900 text-2xl mr-2"
                      icon={faCouch}
                    />
                    Ghế đã đặt
                  </span>
                  <span className="mx-1">
                    <FontAwesomeIcon
                      className="text-green-500 text-2xl mr-2"
                      icon={faCouch}
                    />
                    Ghế bạn chọn{" "}
                  </span>
                </p>
              </div>
            </div>
            <div className="lg:col-span-4 col-span-1">{renderInfoMovie()}</div>
          </div>
        </div>
      ) : (
        <div className="bg-white pt-28">
          <Result
            status="success"
            title="Chúc mừng bạn đã đặt vé thành công!"
            subTitle="*Lưu ý: Vé sau khi đặt không hoàn trả lại được. Xin cảm ơn!"
            extra={[ <button className="bg-primary text-white p-2 hover:text-primary hover:bg-white border" onClick={ () =>{ 
              dispatch(layThongTinPhongChieu(maPhongChieu));
              setVisible(false);
              if(danhSachGheDangDat.length !==0){
                dispatch(createAction(actionType.DEL_BOOKING, []));
              } 
            }}>
              Buy Again
          </button>,
          <NavLink onClick={upHistory} className="border p-2  hover:bg-primary" to="/profile">History Booking</NavLink>]}
          />
        </div>
      )}
    </Layout>
  );
}

export default Booking;
