import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faLock } from "@fortawesome/free-solid-svg-icons";
import {Formik,Field, Form} from 'formik';
import { useDispatch } from "react-redux";
import {signIn} from '../../store/actions/auth';
import { useTranslation } from 'react-i18next';

function SignIn(props) {
  const { setSignIn, isSignIn, setSignUp, setNotification } = props;
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const handleClose = () => {
    setSignIn(false);
  };
   const openNotification = (msg) => {
    setNotification(msg);
  };
  const openSignUp =() =>{
    setSignUp(true);
    handleClose();
  }
  const handleSignIn = (values) => {
     dispatch(signIn(values,handleClose,openNotification));  
  }
  return (
    <>
      {isSignIn ? (
        <>
        <Formik 
            initialValues={{
              taiKhoan:"movietime",
              matKhau:"123456"
            }}
            onSubmit={handleSignIn}
            render={({handleChange})=>(
            <Form className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none ">
            <div className="relative lg:w-2/5 md:w-3/5 w-4/5 my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 className="uppercase text-3xl font-semibold">{t('content.SignIn')}</h3>
                  <button
                    className="p-1 ml-auto border-0 text-black float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={handleClose}
                  >
                    <span className="bg-transparent text-black opacity-20 h-6 w-6 text-2xl block outline-none focus:outline-none">
                      ×
                    </span>
                  </button>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <div className="w-full mb-4">
                    <div className="flex items-center">
                      <i className="ml-3 fill-current text-gray-400 z-10">
                        <FontAwesomeIcon icon={faUser} />
                      </i>
                      <Field
                        type="text"
                        name="taiKhoan"
                        values="movietime"
                        onChange={handleChange}
                        placeholder="Username"
                        className="outline-none shadow -mx-6 px-8 w-full border rounded px-3 py-1 text-gray-700"
                      />
                    </div>
                  </div>

                  <div className="w-full mb-2">
                    <div className="flex items-center">
                      <i className="ml-3 fill-current text-gray-400 z-10">
                        <FontAwesomeIcon icon={faLock} />
                      </i>
                      <Field
                        type="password"
                        name="matKhau"
                        onChange={handleChange}
                        placeholder="Password"
                        className="outline-none shadow -mx-6 px-8 w-full border rounded px-3 py-1 text-gray-700"
                      />
                    </div>
                  </div>

                  <div className="mt-6 flex sm:justify-between justify-end">
                    <div className="items-center sm:flex hidden ">
                      <input type="checkbox" className=" w-4 h-4 mr-2" />
                      <span className=" text-gray-700">{t('content.RememberMe')}</span>
                    </div>
                    <div>
                      <p className=" text-gray-700">
                      {t('content.Donthaveanaccount')}{" "}
                        <span onClick={openSignUp} className=" cursor-pointer text-primary" >
                        {t('content.SignUp')}
                        </span>
                      </p>
                    </div>
                  </div>
                  <button
                    type="submit"
                    className="uppercase hover:opacity-80 w-full mt-6 font-medium bg-primary text-gray-700 rounded px-4 py-2"
                  >
                    {t('content.SignIn')}
                  </button>
                </div>
              </div>
            </div>
          </Form>
            )}        
        />
        </>  
      ) : null}
    </>
  );
}

export default SignIn;
