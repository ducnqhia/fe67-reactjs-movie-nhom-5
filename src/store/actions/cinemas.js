import { createAction } from ".";
import { request } from "../../api/request";
import { actionType } from "./type";

export const fetchCinemaByAddress = (maHeThongRap) =>{
  return(dispatch) => {
    request({
      method:"GET",
      url: "https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinCumRapTheoHeThong",
      params:{
        maHeThongRap:maHeThongRap
      }
    }).then((res) => {      
      dispatch(createAction(actionType.SET_CINEMA_BY_ADDRESS, res.data));
    }).catch((err) => {
      console.log(err.response);
    });
  };
};

export const fetchCinema = (maHeThongRap) => {
  return(dispatch) =>{
    request({
      method:"GET",
      url:"https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuHeThongRap",
      params:{
        maHeThongRap:maHeThongRap,
        maNhom:"GP03",
      }
    }).then((res) => {
      dispatch(createAction(actionType.SET_CINEMA_SCHEDULE, res.data));
    }).catch((err) => {
       console.log(err);
    });
  } 
}

export const fetchCinemaByFilm = (maPhim) => {
  return(dispatch) =>{
    request({
      method:"GET",
      url:"https://movie0706.cybersoft.edu.vn/api/QuanLyRap/LayThongTinLichChieuPhim",
      params:{
        MaPhim:maPhim,
      }
    }).then((res) => {console.log("GET_CINEMA_FILM");
      dispatch(createAction(actionType.GET_CINEMA_FILM, res.data));
    }).catch((err) => {
       console.log(err);
    });
  } 
}