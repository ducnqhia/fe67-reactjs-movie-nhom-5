import { createAction } from ".";
import { request } from "../../api/request";
import { actionType } from "./type";

//async action
export const fetchBanners =  () => {
    return (dispatch) => {
        request({
            method: "GET",
            url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachBanner",
          })
            .then((res) => {
                dispatch(createAction(actionType.SET_BANNER, res.data.content))
            })
            .catch((err) => {
              console.log(err);
            });
    };
};
