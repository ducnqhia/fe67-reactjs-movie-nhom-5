import { request } from "../../api/request";
import { createAction } from "./index";
import { actionType } from "./type";

export const layThongTinPhongChieu = (maLichChieu) => {
    return (dispatch) => {
        request({
            url: "https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/LayDanhSachPhongVe",
            method:"GET",
            params:{
                MaLichChieu:maLichChieu
            }
        })
        .then((res) => {
            console.log("res");
            console.log(res.data); 
            dispatch(createAction(actionType.SET_BOOKING,res.data));
        })
        .catch((err) => {
            console.log("Error");
            console.log(err);
        });
    };
};

export const booking = (data) => {
    return (dispatch) => {
        request({
            url: "https://movie0706.cybersoft.edu.vn/api/QuanLyDatVe/DatVe",
            method:"POST",
            data:data,
        })
        .then((res) => {
            console.log(res.data.content); 
        })
        .catch((err) => {
            console.log(err);
        });
    };
};