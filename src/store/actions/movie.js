import { createAction } from ".";
import { request } from "../../api/request";
import { actionType } from "./type";

//async action
export const fetchMovies =  (tenPhim) => {
    return (dispatch) => {
        request({
          method: "GET",
          // url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim",
          url: "https://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim",
          params:{
            tenPhim :tenPhim,
            maNhom:"GP03",
          }
        })
          .then((res) => {  
            console.log(res.data);
            dispatch(createAction(actionType.SET_MOVIES,res.data));
          })
          .catch((err) => {
            console.log(err);
          });
      };
};
//