import { request } from "../../api/request";
import { createAction } from "./index";
import { actionType } from "./type";

export const signIn = (userLogin, handleCloseSignIn, openNotification) => {
    return (dispatch) => {
        request({
            url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
            method:"POST",
            data:userLogin,
        })
        .then((res) => {     
            localStorage.setItem("token",res.data.accessToken);
            localStorage.setItem("taikhoan",res.data.taiKhoan);
            openNotification("Chúc mừng bạn đã đăng nhập thành công.");
            handleCloseSignIn();
            const taikhoan =  res.data.taiKhoan;
            dispatch( fetchMe(taikhoan));
            console.log(res.data.taiKhoan);
        })
        .catch((err) => {
            openNotification(err.response.data.content);
        });
    };
};

export const signUp = (userSignup, callback,openNotification) => {
 request({
    url:"https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy",
    method:"POST",
    data:userSignup,
 }).then((res) => {
    openNotification("Chúc mừng bạn đã đăng kí thành công tài khoản "+userSignup.taiKhoan +".");
    callback();
 }).catch((err) => {
    openNotification(err.response.data);
 })
};
// export const fetchMe = (dispatch, taiKhoan) => {
//     request({
//         method: "POST",
//         url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
//         data:{
//             taikhoan: taiKhoan,
//         }
//       }).then((res)=>{
//         dispatch(createAction(actionType.SET_USER, res.data));
//       }).catch((err)=>{
//         console.log(err);
//       });
//   };

export const fetchMe = (taiKhoan) => {
    return (dispatch) => {
        request({
                    method: "POST",
                    url: "https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan",
                    data:{
                        taikhoan: taiKhoan,
                    }
                  }).then((res)=>{
                    dispatch(createAction(actionType.SET_USER, res.data));
                  }).catch((err)=>{
                    console.log(err);
                  })
    }  
 };
  export const updateUser = (userUpdate,info) => {
    return (dispatch) => {
    request({
       url:"https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
       method:"PUT",
       data:userUpdate,
    }).then((res) => {
        console(res.data);
        // fetchMe(dispatch("movietime"));
        info("Chúc mừng bạn đã cập nhập thành công tài khoản "+userUpdate.taiKhoan +".");
    }).catch((err) => {
        console.log("errr");
        console.log(err.response);
        info(err.response?.data);
    })}
   };
  