import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import movie from "./reducers/movie";
import user from "./reducers/user";
import carousel from './reducers/carousel';
import  cinemas from './reducers/cinemas';
import booking from './reducers/booking';
import thunk from "redux-thunk";

const reducer = combineReducers({
    movie,
    user,
    carousel,
    cinemas,
    booking,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(reducer, composeEnhancers(applyMiddleware(thunk)));