import { actionType } from "../actions/type";

const initialState = {
  movieList: [],
  movieListDefault: [],
  all: true,
  sapChieu: false,
  dangChieu: false,
  phimHot: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_MOVIES:
      state.movieList = action.payload;
      state.movieListDefault = action.payload;
      return { ...state };
    case actionType.SET_MOVIES_ALL:
      state.all = true;
      state.phimHot = false;
      // state.sapChieu = false;
      // state.dangChieu = false;
      state.movieList = [...state.movieListDefault];
      return { ...state };
    // case actionType.SET_MOVIES_INTHEATER:
    //   state.all = false;
    //   state.sapChieu = false;
    //   state.dangChieu = true;
    //   state.movieList = state.movieListDefault.filter(
    //     (film) => film.dangChieu === true
    //   );
    //   return { ...state };
    // case actionType.SET_MOVIES_COMINGSOON:
    //   state.all = false;
    //   state.sapChieu = true;
    //   state.dangChieu = false;
    //   state.movieList = state.movieListDefault.filter(
    //     (film) => film.sapChieu === true
    //   );
    //   return { ...state };
      case actionType.SET_MOVIES_HOT:
        state.all = false;
        state.phimHot = true;
        // state.dangChieu = false;
        state.movieList = state.movieListDefault.filter(
          (film) => film.danhGia  >= 10
        );
        return { ...state };
    case actionType.GET_MOVIES_INFO:
      state.moiveInfo = action.payload;
      return { ...state };
    default:
      return state;
  }
};

export default reducer;
