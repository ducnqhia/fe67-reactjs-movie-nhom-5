import { actionType } from "../actions/type";

const initialState = {
  cinemaList: [],
  cinemaMovie: [],
  cinemaByAddress: null,
  cinemaSchedule: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.GET_CINEMA_FILM:
      const videoID = action.payload.trailer.split('/');
      state.cinemaMovie = {...action.payload, trailer:videoID[videoID.length-1]};
      return { ...state };
    case actionType.SET_CINEMA_SCHEDULE:
      state.cinemaList = action.payload;
      return { ...state };
    case actionType.DEL_MOVIES:
      state.cinemaMovie = action.payload;
      return { ...state };
    default:
      return state;
  }
};

export default reducer;
