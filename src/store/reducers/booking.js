import { actionType } from "../actions/type";

const initialState = {
  listBooking: null,
  danhSachGheDangDat: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_BOOKING:
      state.listBooking = action.payload;
      return { ...state };
    case actionType.ADD_SEAT:
      let danhSachGheCapNhap = [...state.danhSachGheDangDat];
      let index = danhSachGheCapNhap.findIndex(
        (item) => item.maGhe === action.payload.maGhe
      );
      if (index !== -1) {
        danhSachGheCapNhap.splice(index, 1);
      } else {
        danhSachGheCapNhap.push(action.payload);
      }
      state.danhSachGheDangDat = action.payload;
      return { ...state, danhSachGheDangDat: danhSachGheCapNhap };
    case actionType.DELETE_SEAT:
      state.listBooking = action.payload;
      return { ...state };
    case actionType.DEL_BOOKING:
      state.danhSachGheDangDat = action.payload;
      return { ...state };
    default:
      return { ...state };
  }
};

export default reducer;
