// import React, {useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import { fetchBanners } from "../../store/actions/carousels";
// import { useDispatch } from "react-redux";
// import { useSelector } from "react-redux";
import "./styles.css";
import images from '../../assets';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay } from "@fortawesome/free-solid-svg-icons";
import "./modal-video.min.css";
import ModalVideo from 'react-modal-video';
import { useState } from "react";

function CarouselBanners(props) {
  const {Banner1, Banner2, Banner3} = images;
  // const dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(fetchBanners()) ;
  // }, [dispatch]);
  
//   const carousels = useSelector((state) => {
//     return state.carousel || [];
// });
  const settings = {
    className: "section-outstanding__slider",
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 2000,
    dots:true,
  };
  const [isOpen, setOpen] = useState(false);
  const [isOpen1, setOpen1] = useState(false);
  const [isOpen2, setOpen2] = useState(false);
  return (
    <div className="movie-carousel w-full">
    <Slider {...settings}>
    <div  className="movie-carousel-item"> 
           <FontAwesomeIcon className="btn-play-trailer" onClick={()=> setOpen(true)} icon={faPlay} />
            <img className="w-full" src={Banner1} alt="" /> 
          </div>
          <div  className="movie-carousel-item"> 
          <FontAwesomeIcon className="btn-play-trailer" onClick={()=> setOpen1(true)} icon={faPlay} />
            <img className="w-full" src={Banner2} alt="" /> 
          </div>
          <div  className="movie-carousel-item"> 
          <FontAwesomeIcon className="btn-play-trailer" onClick={()=> setOpen2(true)} icon={faPlay} />
            <img className="w-full" src={Banner3} alt="" /> 
          </div>
      {/* {carousels.map((item, index) => {
        return (
         <div key={index} className="movie-carousel-item"> 
            <img className="w-full" src={item.hinhAnh} alt="" /> 
          </div>
        );
      })} */}
    </Slider>
    <ModalVideo channel='youtube'  youtube={{mute:1,autoplay:1}} isOpen={isOpen} videoId="qnDLYACgd38" onClose={() => setOpen(false)} />
    <ModalVideo channel='youtube' youtube={{mute:1,autoplay:1}}  isOpen={isOpen1} videoId="F-78MafLjLY" onClose={() => setOpen1(false)} />
    <ModalVideo channel='youtube' youtube={{mute:1,autoplay:1}}  isOpen={isOpen2} videoId="UZOnOfc83Q0" onClose={() => setOpen2(false)} />
    </div>
  );
}

export default CarouselBanners;
