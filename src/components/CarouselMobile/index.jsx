import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slide1 from "../../assets/slide1.jpg";
import Slide2 from "../../assets/slide2.jpg";
import Slide3 from "../../assets/slide3.jpg";

const CarouselMobile = (props) => {

  const settings = {
    className: "section-outstanding__slider",
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 2000,
  };
  return (
    <Slider id="movie-app-mobile" {...settings}>
      <div>
        <img className="w-full mx-auto" src={Slide1} alt="" />
      </div>
      <div>
        <img className="w-full mx-auto" src={Slide2} alt="" />
      </div>
      <div>
        <img className="w-full mx-auto" src={Slide3} alt="" />
      </div>
    </Slider>
  );
};

export default CarouselMobile;
