import React from "react";
import "./styles.css";
import Mobile from "../../assets/mobile.png";
import CarouselMobile from "../CarouselMobile";
import { faDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";
import { useTranslation } from 'react-i18next';

function Application(props) {
  const { t } = useTranslation();
  return (
    <div id="app" className="bg-app mb-6">
      <div className="grid md:grid-cols-2 grid-cols-1 md:py-24 py-12 container gap-12 items-center">
        <div>
          <h3 className="text-justify text-white font-medium text-4xl">
          {t('content.Favoriteapp')}
          </h3>
          <p className=" text-lg text-justify text-white py-6">
          {t('content.Notonlybookingtickets')}
          </p>
        
        <div className="text-center">
            {/* <span className="text-2xl my-auto text-white py-6 w-1/2"> */}
              <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
                {({ isVisible }) => (
                  <p className="sm:text-5xl text-4xl font-bold shadow-2xl">
                    +{isVisible ? <CountUp start={1000} end={9999} /> : 9999}{" "}
                    {t('content.DOWLOADED')}
                  </p>
                )}
              </VisibilitySensor>
            {/* </span> */}
            <button
              className="p-2  text-white font-medium text-xl lg:w-1/2 md:w-3/4 sm:w-1/2 w-3/4"
              style={{ backgroundColor: "#ff6600" }}
            >
              <FontAwesomeIcon className="mx-1" icon={faDownload} />{t('content.DOWLOADNOW')}
            </button>
            </div>
        </div>
        <div className="mx-auto caoursel-mobile">
          <CarouselMobile />
          <img className=" mx-auto caoursel-frame" src={Mobile} alt="" />
        </div>
      </div>
    </div>
  );
}

export default Application;
