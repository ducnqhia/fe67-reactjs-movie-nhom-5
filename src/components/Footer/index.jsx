import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faFacebookF,faTwitter,faInstagram,} from "@fortawesome/free-brands-svg-icons";
import {faMapMarkerAlt,faPhoneSquare,faEnvelope,} from "@fortawesome/free-solid-svg-icons";
import images from '../../assets';
import "./styles.css";
import { useTranslation } from 'react-i18next';


function Footer(props) {
  const { t } = useTranslation();
  const { Logo,BT,BHD,CGV, Cinestar,CNX,Dcine,Glaxy,Lotte,Mega,Startlight,Touch,Apple,Google} = images;
  return (
    <div id="contact" className="movie-footer">
      <div className="container mb-4">
        <div className="grid md:grid-cols-12 sm:grid-cols-2 grid-cols-1 gap-x-4">
          <div className="md:col-span-3">
            <h2 className="py-2 font-semibold uppercase tracking-widest leading-10 ">
            {t('content.ABOUTUS')}
            </h2>
            <a href=".">
              <img
                className="mx-auto"
                src={Logo}
                style={{ width: "100px" }}
                alt="Logo Movie Time"
              />
            </a>
            <p className="text-justify pt-1 text-secondary">
            {t('content.MovieTime')}
            </p>
          </div>
          <div className="md:col-span-3">
            <h2 className="py-2 font-semibold uppercase tracking-widest leading-10 ">
            {t('content.PARTNERS')}
            </h2>
            <div className="grid grid-cols-3 gap-y-4">
              <a href="https://www.bhdstar.vn/">
                <img className="w-3/6 bg-white rounded-full" src={BHD} alt="" />
              </a>
              <a href="https://www.betacinemas.vn/">
                <img className="w-3/6 bg-white rounded-full" src={BT} alt="" />
              </a>
              <a href="https://www.cgv.vn/">
                <img className="w-3/6 bg-white rounded-full" src={CGV} alt="" />
              </a>
              <a href="https://cinestar.com.vn/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Cinestar}
                  alt=""
                />
              </a>
              <a href="https://cinemaxvn.com/">
                <img className="w-3/6 bg-white rounded-full" src={CNX} alt="" />
              </a>
              <a href="https://www.dcine.vn/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Dcine}
                  alt=""
                />
              </a>
              <a href="https://touchcinema.com/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Touch}
                  alt=""
                />
              </a>
              <a href="https://www.galaxycine.vn/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Glaxy}
                  alt=""
                />
              </a>
              <a href="https://lottecinemavn.com/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Lotte}
                  alt=""
                />
              </a>
              <a href="http://starlight.vn/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Startlight}
                  alt=""
                />
              </a>
              <a href="https://www.megagscinemas.vn/">
                <img
                  className="w-3/6 bg-white rounded-full"
                  src={Mega}
                  alt=""
                />
              </a>
            </div>
          </div>
          <div className="md:col-span-2">
            <h2 className="py-2 font-semibold uppercase tracking-widest leading-10">
            {t('content.SOCIAL')}
            </h2>
            <ul className="leading-relaxed list-none text-secondary">
              <li className="capitalize">
                <a href="https://www.facebook.com/">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faFacebookF}
                  />facebook
                </a>
              </li>
              <li className="capitalize">
                <a href="https://twitter.com/">
                  <FontAwesomeIcon style={{ width: "20px", marginRight: "5px" }} icon={faTwitter} />twitter
                </a>
              </li>
              <li className="capitalize">
                <a href="https://www.instagram.com/">
                  <FontAwesomeIcon
                   style={{ width: "20px", marginRight: "5px" }}
                    icon={faInstagram}
                  />instagram
                </a>
              </li>
            </ul>
          </div>
          <div className="md:col-span-4">
            <h2 className=" py-2 font-semibold uppercase tracking-widest leading-10 ">
            {t('content.contact')}
            </h2>
            <ul className="text-justify leading-relaxed list-none text-secondary">
              <li className="capitalize ">
                <p className="m-0 hover:text-white cursor-pointer">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faMapMarkerAlt}
                  />{t('content.ADDRESS')} 
                </p>
              </li>
              <li className="capitalize">
                <a href="tel:+84987654321">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faPhoneSquare}
                  />098.765.4321
                </a>
              </li>
              <li>
                <a href="mailto:movietime@gmail.com">
                  <FontAwesomeIcon
                    style={{ width: "20px", marginRight: "5px" }}
                    icon={faEnvelope}
                  />movietime@gmail.com
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <hr className="my-6" />
      <div className="container mb-4">
        <div className="grid md:grid-cols-3 grid-cols-1 text-secondary">
          <p className="md:text-left text-center py-2 my-auto">
            <a href="." >{t('content.TermsAndPolicies')}</a>
          </p>
          <p className="md:block hidden  py-2 my-auto">2021 © Movie Time</p>
          <div className="py-2 flex justify-between">
            <a href="https://www.apple.com/app-store/" >
              <img className="w-4/5" src={Apple} alt="Apple Store" />
            </a>
            <a href="https://play.google.com/store" >
              <img className="ml-auto w-4/5" src={Google} alt="Google Store" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Footer;
