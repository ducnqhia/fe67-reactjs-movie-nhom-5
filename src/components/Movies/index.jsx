import React, { useEffect, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilm, faPlay } from "@fortawesome/free-solid-svg-icons";
import "./styles.css";
import { fetchMovies } from "../../store/actions/movie";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import images from "../../assets";
import Slider from "react-slick";
import { NavLink} from "react-router-dom";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { useTranslation } from 'react-i18next';

function Movies(props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  
  useEffect(() => {
    dispatch(fetchMovies());
  }, [dispatch]);
  const {all, phimHot } = useSelector((state) => {
    return state.movie || [];
  });;
  const {ImgNotFound, Hot} = images;
  const ref = useRef({});
  const settings = {
    dots: false,
    infinite: false,
    rows: 2,
    slidesToShow: 4,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1280,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };
  
  const fetchMovieAll = () =>{
    dispatch(createAction(actionType.SET_MOVIES_ALL));
  }
  // const fetchMovieComingsoon = () =>{
  //   dispatch(createAction(actionType.SET_MOVIES_COMINGSOON));
  // }
  // const fetchMovieInTheater = () =>{
  //   dispatch(createAction(actionType.SET_MOVIES_INTHEATER));
  // }
  const fetchMovieHot = () =>{
    dispatch(createAction(actionType.SET_MOVIES_HOT));
  }
  const movieList = useSelector((state) => {
    return state.movie.movieList || [];
  });

  const renderMovie = () => {
    let moviesLength = movieList.length;
    if(moviesLength%2!== 0){
      moviesLength -=1;
    }
    return movieList.slice(0,moviesLength).map((item) => {
      return (
        <div key={item.maPhim} className="movies-card mt-6 px-3">
          {item.danhGia >=10 ? (
            <img className="movies-ribbon h-24" src= {Hot} alt="Hot" />
          ) : (
            ""
          )}
          <div className="movies-img">
            <img
              className="w-full h-80"
              src={item.hinhAnh}
              alt={item.tenPhim}
              onError={(e) => {
                e.target. src=ImgNotFound  //replacement image imported above
                e.target.style = 'height:20rem; width:100%' // inline styles in html format
            }}
            />
            <div className="overlay" />
            <div className="movies-play">
              <NavLink to={`/detail/${item.maPhim}` }>
              <FontAwesomeIcon className="movies-button" icon={faPlay} />
              </NavLink>
              <p className="uppercase  text-gray-700 font-bold tracking-wide mt-2">
              {t('content.BOOKINGNOW')}
              </p>
              <p className="movies-desc text-gray-700 font-normal text-justify py-1">
                {item.moTa}
              </p>
              <p className="text-white">
              {t('content.Released')}: {moment(item.ngayKhoiChieu).format("hh:mm A DD/MM ")}
              </p>
            </div>
          </div>
          <h3 className="movies-name uppercase mt-2 font-bold tracking-widest text-md">
            {item.tenPhim}
          </h3>
          <div>
              <span className="movies-overerate">
                {
                  <>
                    {[...Array(item.danhGia)].map((e, i) => {
                      return (
                        <img
                          className="sm:w-4 w-3 inline"
                          src="https://img.icons8.com/fluency/48/000000/star.png"
                          alt="Rate"
                        />
                      );
                    })}
                  </>
                }
                {item.danhGia < 10 ? (
                  <>
                    {[...Array(10 - item.danhGia)].map((e, i) => {
                      return (
                        <img
                          className="sm:w-4 w-3"
                          src="https://img.icons8.com/color/48/000000/star--v1.png"
                          alt="Rate"
                        />
                      );
                    })}
                  </>
                ) : (
                  ""
                )}
              </span>
            </div>
          {/* <div>
            <Rate disabled defaultValue={item.danhGia} count={10} />
          </div> */}
        </div>
      );
    });
  };
 
  let classActiveAll = all===true?"text-white":"";
  // let classActiveDC = dangChieu===true?"text-white":"";
  // let classActiveSC = sapChieu===true?"text-white":"";
  let classActiveHOT = phimHot===true?"text-white":"";

  return (
    <div id="movies">
      <div className="movies container pb-8 ">
        <h2 className="border-b-2 border-primary uppercase tracking-widest font-normal text-2xl py-4">
          <FontAwesomeIcon className="mr-3" icon={faFilm} />
          {t('content.MOVIELIST')}
        </h2>
      
        <p className="p-3 pb-0 movies-button">
          <button className={`${classActiveAll}`} onClick={fetchMovieAll}>{t('content.ALL')}</button>
          <span className="px-2">|</span><button className={`${classActiveHOT}`} onClick={fetchMovieHot}>{t('content.FILMHOT')} </button>
          {/* <span className="px-2">|</span><button className={`${classActiveDC}`} onClick={fetchMovieInTheater}>IN THREATER</button>
          <span className="px-2">|</span><button className={`${classActiveSC}`} onClick={fetchMovieComingsoon}>COMING SOON</button> */}
        </p>
        <Slider ref={ref} {...settings}>
          {renderMovie()}
        </Slider>
      </div>
    </div>
  );
}

export default Movies;
