import moment from "moment";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink} from "react-router-dom";
import { message} from 'antd';
import images from '../../assets';
import { useTranslation } from 'react-i18next';

function CinemasSchedule(props) {
  const { tenPhim, hinhAnh, hot, dangChieu, lstLichChieuTheoPhim } = props.item;
  const { imgDefault} = images;
  const { t } = useTranslation();
  
  const user = useSelector((state) => {
    return state.user || [];
  });
  
  const warning = () => {
    message.warning('Vui lòng đăng nhập để đặt vé xem phim', 4);
  };

  return (
    <div className="mb-2 border-b border-gray-100 md:px-6 pl-3 pr-6">
      <div className="sm:flex block  cinema-desc">
        <img className="w-8 h-12 sm:mr-2 sm:mx-0 mx-auto" src={hinhAnh} alt={tenPhim}  onError={(e) => {
                        e.target.src = imgDefault 
                        e.target.style = '' 
                    }} />
        <div>
          <div className="sm:text-justify text-center">
            <h3 className="mr-2 capitalize font-bold">{tenPhim}</h3>
          </div>
          <div className="flex mb-2 text-white text-xs sm:justify-start justify-center">
            {hot ? (
              <span className="sm:text-md text-xs bg-red-500 sm:mr-2 mr-1 sm:p-1 p-0.5 rounded sm:font-bold font-medium">HOT</span>
            ) : (
              <div></div>
            )}
            {dangChieu ? (
              <div className="sm:text-md text-xs bg-gray-500 sm:mr-2 mr-1 sm:p-1 p-0.5 rounded">Coming Soon</div>
            ) : (
              <div className="sm:text-md text-xs bg-green-500 sm:mr-2 mr-1 sm:p-1 p-0.5 rounded">{t('content.InTheaters')}</div>
            )}
          </div>
        </div>
      </div>
      <div className="grid place-items-center 2xl:grid-cols-6 xl:grid-cols-5 lg:grid-cols-4 sm:grid-cols-3 grid-cols-2 sm:gap-4 gap-1  my-2 ">
        {lstLichChieuTheoPhim.slice(0, 12).map((item) => {
          return (
            <div>
              {Object.keys(user).length>0?
              <NavLink to={`/booking/${item.maLichChieu}`}
                className="transition-all duration-500 hover:bg-primary hover:text-white hover:ring-2 border border-gray-100 p-1 sm:text-sm text-xs text-gray-700 text-center rounded"
                key={item.maLichChieu}
              >
                <span>{moment(item.ngayChieuGioChieu).format("hh:mm A")}</span>
              </NavLink>:
               <NavLink to="" onClick={warning}
               className="transition-all duration-500 hover:bg-primary hover:text-white hover:ring-2 border border-gray-100 p-1 sm:text-sm text-xs text-gray-700 text-center rounded"
               key={item.maLichChieu}
             >
               <span>{moment(item.ngayChieuGioChieu).format("hh:mm A")}</span>
             </NavLink>
              }
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default CinemasSchedule;
