import { faCalendarAlt } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Tabs } from "antd";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchCinema } from "../../store/actions/cinemas";
import CinemasSchedule from "../CinemasSchedule";
import "antd/dist/antd.css";
import "./styles.css";
import { useTranslation } from 'react-i18next'  

const { TabPane } = Tabs;

function ShowTimes(props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const cinemaList = useSelector((state) => {
    return state.cinemas.cinemaList || [];
  });

  useEffect(() => {
    dispatch(fetchCinema());
  }, [dispatch]);

  return (
    <div id="showtimes" className=" movie-showtime">
      <div className="container">
        <h2 className="border-b-2 border-primary uppercase tracking-widest font-normal text-2xl py-4">
          <FontAwesomeIcon
            style={{ marginRight: "10px" }}
            icon={faCalendarAlt}
          />
          {t('content.MOVIESHOWTIMES')}
        </h2>
        <Tabs className="shadow" defaultActiveKey="1" centered>
          {cinemaList.map((heThongRap) => {
            return (
              <TabPane 
                tab={
                  <img
                    className="w-20 cursor-pointer bg-white rounded-full shadow-md"
                    src={heThongRap.logo}
                    alt={heThongRap.tenHeThongRap}
                  />
                }
                key={heThongRap.maHeThongRap}
              >
                <Tabs
                id="showtime-schedule"
                  className="h-80"
                  tabPosition="left"
                  defaultActiveKey="1"
                  centered
                >
                  {heThongRap.lstCumRap.map((cumRap) => {
                    return (
                      <TabPane
                        key={cumRap.maCumRap}
                        tab={ 
                          <div className=" mx-2 p-2 flex">
                            <img
                              className="  content-center w-10 h-10 cursor-pointer shadow bg-white rounded-full cinema-logo mr-2"
                              src={heThongRap.logo}
                              alt={heThongRap.tenHeThongRap}
                            />
                            {/* <div className="showtime-cinema-name"> */}
                              <p className="font-bold cinemas-name">{cumRap.tenCumRap}</p>
                              {/* <p >{cumRap.diaChi}</p> */}
                            {/* </div> */}
                          </div>
                        }
                      >
                        <div className="h-80">
                          <div className="flex px-4 py-1 mb-2 bg-gray-200">
                          <img
                              className=" content-center w-10 h-10 cursor-pointer shadow bg-white rounded-full cinema-logo mr-2"
                              src={heThongRap.logo}
                              alt={heThongRap.tenHeThongRap}
                            />
                            <div>
                            <h3 className="font-bold my-0 ">{t('content.Movieshowtimesin')} {cumRap.tenCumRap}</h3>
                            <p  className="my-0 ">{cumRap.diaChi}</p>
                            </div>
                          </div>
                          {cumRap.danhSachPhim.map((film) => {
                            return (
                              <CinemasSchedule key={film.maPhim} item={film} />
                            );
                          })}
                        </div>
                      </TabPane>
                    );
                  })}
                </Tabs>
              </TabPane>
            );
          })}
        </Tabs>
      </div>
    </div>
  );
}

export default ShowTimes;
