import React, { useState } from "react";
import Logo from "../../assets/logo.png";
import SignUp from "../../views/SignUp";
import SignIn from "../../views/SignIn";
import { useDispatch, useSelector } from "react-redux";
import { createAction } from "../../store/actions";
import { actionType } from "../../store/actions/type";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faSignInAlt,
  faSignOutAlt,
  faUserPlus,
} from "@fortawesome/free-solid-svg-icons";
import Notification from "../../views/Notification";
import { NavLink } from "react-router-dom";
import "./styles.css";
import { Avatar, Anchor, Switch } from "antd";
import { UserOutlined } from "@ant-design/icons";
import { Popover } from "antd";
import { useTranslation } from 'react-i18next';
import i18n from '../../translation/i18n';

function Header(props) {
  const [isSignIn, setSignIn] = useState(false);
  const [isSignUp, setSignUp] = useState(false);
  const [isNotification, setNotification] = useState("false");
  
  const dispatch = useDispatch();

  const { t } = useTranslation()

  const { taiKhoan } = useSelector((state) => {
    return state.user || [];
  });
  const cinemaMovie = useSelector((state) => {
    return state.cinemas.cinemaMovie || [];
  });
  const danhSachGheDangDat = useSelector((state) => {
    return state.booking.danhSachGheDangDat || [];
  });
  const handleSignOut = () => {
    localStorage.clear();
    dispatch(createAction(actionType.DEL_USER, []));
    if (cinemaMovie.length !== 0) {
      dispatch(createAction(actionType.DEL_MOVIES, []));
    }
    if (danhSachGheDangDat.length !== 0) {
      dispatch(createAction(actionType.DEL_BOOKING, []));
    }
  };
  const goHomePage = () => {
    if (cinemaMovie.length !== 0) {
      dispatch(createAction(actionType.DEL_MOVIES, []));
    }
    if (danhSachGheDangDat.length !== 0) {
      dispatch(createAction(actionType.DEL_BOOKING, []));
    }
  };

 const changeLanguage = (e) => {
   if(e===false)
   {
    i18n.changeLanguage("vi");
   }
   else{
   i18n.changeLanguage("en");
  }
}
  const content = (
    <div>
      <NavLink to="/profile" className="header-profile hover:text-primary">
        {" "}
        <FontAwesomeIcon className="mr-1" icon={faUser} />{t('content.Profile')}
      </NavLink>
      <br />
      <NavLink
        to="/"
        className="header-profile mt-1 hover:text-primary"
        onClick={handleSignOut}
      >
        <FontAwesomeIcon className="mr-1 " icon={faSignOutAlt} />
        <span>{t('content.SignOut')}</span>
      </NavLink>
    </div>
  );
  const { Link } = Anchor;
  return (
    <div className="movie-header">
      <div className="container flex justify-between py-3 ">
        <NavLink onClick={goHomePage} to="">
          <img src={Logo} className="w-20" alt="Logo Movie Time" />
        </NavLink>
        <div className="md:block hidden text-white my-auto">
          <Anchor className=" uppercase">
            <Link href="#movies" title={t('content.movies')} />
            <Link href="#showtimes" title={t('content.showtimes')} />
            <Link href="#app" title={t('content.app')} />
            <Link href="#contact" title={t('content.contact')} />
          </Anchor>
        </div>
        <Switch onChange={changeLanguage} checkedChildren="EN" unCheckedChildren="VI" defaultChecked />
        {!taiKhoan ? (
          <div className="my-auto lg:ml-12 md:ml-2 ml-12 border border-gray-700 p-2">
            <button onClick={() => setSignIn(true)}>
              <FontAwesomeIcon icon={faSignInAlt} />
              <span className="sm:inline hidden"> {t('content.SignIn')}</span>
            </button>
            <span className="px-1">/</span>
            <button onClick={() => setSignUp(true)}>
              <FontAwesomeIcon icon={faUserPlus} />
              <span className="sm:inline hidden mr-1"> {t('content.SignUp')}</span>
            </button>
          </div>
        ) : (
          <div className="my-auto lg:ml-12 md:ml-2 ml-12">
            <Popover placement="bottom" content={content} trigger="click">
              <button className="mr-2">
                <Avatar
                  style={{ backgroundColor: "#FF9900" }}
                  icon={<UserOutlined />}
                />{" "}
                {t('content.Hello')}! {taiKhoan}
              </button>
            </Popover>
          </div>
        )}

        <SignUp
          isSignUp={isSignUp}
          setSignUp={setSignUp}
          setSignIn={setSignIn}
          setNotification={setNotification}
        />
        <SignIn
          isSignIn={isSignIn}
          setSignIn={setSignIn}
          setSignUp={setSignUp}
          setNotification={setNotification}
        />
        <Notification
          isNotification={isNotification}
          setNotification={setNotification}
        />
      </div>
    </div>
  );
}

export default Header;
